<?php
  if (isset($_GET["cc"])) {
    setcookie("missions", "", time() - 3600); //clear cookie
    header("Location: /");
    die;
  }

  if (isset($_GET["missionid"]) && isset($_GET["description"])) {
    if (isset($_COOKIE["missions"])) {
      $x = json_decode($_COOKIE["missions"], true);
      $x[$_GET["missionid"]] = [
        "description" => $_GET["description"],
        "startinggrid" => $_GET["startinggrid"],
        "priority" => $_GET["priority"],
        "endinggrid" => $_GET["endinggrid"]
      ];
      setcookie("missions", json_encode($x), time() + (86400 * 30), "/");
    } else {
      setcookie("missions", json_encode([$_GET["missionid"] => [
        "description" => $_GET["description"],
        "startinggrid" => $_GET["startinggrid"],
        "priority" => $_GET["priority"],
        "endinggrid" => $_GET["endinggrid"]
      ]]), time() + (86400 * 30), "/");
    }
    header("Location: /");
    die;
  }

  echo "<!--" . file_get_contents("./LICENSE") . "-->";
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="icon" type="image/png" href="icon.png"/>
    <title>Aviation Mission Tracker</title>
    <link rel="stylesheet" href="awsm.min.css"/>
  </head>
  <body>
    <form>
      <label for="missionid">MISSION ID:</label>
      <input id="missionid" name="missionid" type="text" placeholder="H4" class="form-control" autofocus>
      <label for="description">DESCRIPTION:</label>
      <input id="description" name="description" type="text" placeholder="fast-rope section to LZ" class="form-control">
      <label for="startinggrid">START GRID:</label>
      <input id="startinggrid" name="startinggrid" type="text" placeholder="012056 (must be 6-digits!)" class="form-control" value="<?php echo end(json_decode($_COOKIE["missions"], true))["startinggrid"]; ?>">
      <label for="endinggrid">END GRID:</label>
      <input id="endinggrid" name="endinggrid" type="text" placeholder="034098 (must be 6-digits!)" class="form-control">
      <label for="priority">PRIORITY:</label>
      <input id="priority" name="priority" type="text" placeholder="1-10 (10 is highest)" class="form-control">
      <input type="submit" class="btn btn-primary btn-block"/>
      <a href="?cc" class="btn btn-warning btn-block">Clear Missions</a>
    </form>
    <table>
      <tr>
        <th>ID</th>
        <th>DESCRIPTION</th>
        <th>GRIDS</th>
        <th>DISTANCE</th>
        <th>ETA</th>
        <th>PRIORITY</th>
      </tr>
      <?php
        if (isset($_COOKIE["missions"])) {
          foreach (json_decode($_COOKIE["missions"], true) as $key => $value) {
            if ($value["startinggrid"] && $value["endinggrid"]) {
              $averageSpeed = 150; //in km/h (kph)
              $start_X = (int)substr($value["startinggrid"], 0, -3);
              $start_Y = (int)substr($value["startinggrid"], 3, 6);
              $end_X = (int)substr($value["endinggrid"], 0, -3);
              $end_Y = (int)substr($value["endinggrid"], 3, 6);

              $diff_X_raw = $start_X - $end_X;
              $diff_X_pos = ($diff_X_raw < 0) ? $diff_X_raw * -1 : $diff_X_raw ;
              $diff_Y_raw = $start_Y - $end_Y;
              $diff_Y_pos = ($diff_Y_raw < 0) ? $diff_Y_raw * -1 : $diff_Y_raw ;

              $gridDiff = sqrt($diff_X_pos**2 + $diff_Y_pos**2) * 100; //difference in meters

              $eta = $gridDiff / ($averageSpeed*1000/60/60); //eta in seconds

              var_dump();
            }

            echo "<tr><td>" . $key . "</td><td>" .
              $value["description"] . "</td><td>" .
              $value["startinggrid"]."-".$value["endinggrid"] . "</td><td>" .
              round($gridDiff) . "m </td><td>" .
              floor($eta / 60 % 60) . "m " . floor($eta % 60) . "s </td><td>" .
              $value["priority"] . "</td>" .
              "</tr>";
          }
        }
      ?>
    </table>
  </body>
</html>
