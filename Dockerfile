FROM php:7.2-cli
COPY ./src/ /app/
COPY ./LICENSE /app/LICENSE
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"
EXPOSE 5000/tcp
CMD [ "php", "-S", "0.0.0.0:5000", "-t", "/app/"]
